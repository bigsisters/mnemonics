import java.io.File
import java.util.Scanner

import scala.collection.mutable
import scala.util.Try


trait Node {
  val word:String
  val length:Int
  val successors:mutable.Map[Int, List[(Node, Int)]]
  def addNode(node:Node):Node
}

class NilNode extends Node {
  val word: String = ""
  val length: Int = 0
  val successors: mutable.Map[Int, List[(Node, Int)]] = mutable.Map.empty
  def addNode(node: Node): Node = node
}

class NodeImpl(nodeWord:String, nodeSuccessors:mutable.Map[Int, List[(Node, Int)]]) extends Node{
  val word: String = nodeWord
  val length: Int = nodeWord.length
  val successors = nodeSuccessors

  def update(newNode:Node, successors:List[(Node, Int)]):List[(Node,Int)] = successors match {
    case Nil =>  List((newNode, 1))
    case head::tail =>
      if (head._1.word == newNode.word) (head._1, head._2 + 1) +: tail
      else head +: update(newNode, successors.tail)
  }

  def addNode(node:Node): Node = {
    val len = node.length
    successors(len) = update(node, successors.getOrElse(len, Nil))
    this
  }
}


trait MarkovChain {
  val nodeMap:mutable.Map[Int, List[Node]]
  def addWord(word:String):MarkovChain

  def getMaxAcc(list:List[(Node, Int)], maxVal:Int, maxWordNode:Node):Node = list match {
      case Nil => maxWordNode
      case head::tail => if (head._2 > maxVal) getMaxAcc(tail, head._2, head._1) else getMaxAcc(tail, maxVal, maxWordNode)
  }


  def getMax(list:List[(Node, Int)]):Node = list match  {
    case Nil => new NodeImpl("*",mutable.Map.empty)
    case head::tail => getMaxAcc(list.tail, list.head._2, list.head._1)
  }

  def buildAcc(currentNode:Node, numberString:String, acc:List[String]):List[String] = numberString match {
      case "" => acc
      case _ =>
        try {
          val num = Integer.parseInt(numberString.head + "")
          val newNode = getMax(currentNode.successors.getOrElse(num, Nil))
          if (newNode.word == "*") (acc :+newNode.word) ++ buildNewText(numberString.tail)
          else
            buildAcc(newNode, numberString.tail, acc :+ newNode.word)
        } catch {
          case e:NumberFormatException => buildAcc(currentNode, numberString.tail, acc)
        }
  }


  def buildNewText(numberString:String):List[String] = {
    if (numberString == "") Nil
    else {
      val firstNum = Integer.parseInt(numberString.head + "")
      val firstNumList: List[Node] = nodeMap.getOrElse(firstNum, Nil)
      nodeMap.getOrElse(firstNum, Nil) match {
        case Nil => "*" +: buildNewText(numberString.tail)
        case head::tail => buildAcc(head, numberString.tail, List(head.word) )
      }
    }
  }


}

class EmptyChain extends MarkovChain {
  val nodeMap: mutable.Map[Int, List[Node]] = mutable.Map.empty

  def addWord(word: String): MarkovChain = {
    val currentNode = new NodeImpl(word, mutable.Map.empty)
    nodeMap(word.length) = List(currentNode)
    new MarkovChainImpl(currentNode, nodeMap)
  }
}

class MarkovChainImpl(var current:Node, val nodeMap:mutable.Map[Int, List[Node]]) extends MarkovChain {

  def addWord(word: String): MarkovChain = {
    nodeMap.get(word.length) match {
      case None =>
        val newCurrentNode = new NodeImpl(word, mutable.Map.empty)
        current.addNode(newCurrentNode)
        nodeMap(word.length) = List(newCurrentNode)
        current = newCurrentNode
        this
      case Some(nodeList) =>
        val needed = nodeList.filter(_.word == word) match {
          case Nil =>
            val newNode = new NodeImpl(word, mutable.Map.empty)
            nodeMap(word.length) = nodeList :+ newNode
            newNode
          case head::tail => head
        }
        current.addNode(needed)
        current = needed
        this
    }
  }
}


object MnemoGenerator extends App {
  def feedSourceText(sourcePath:String):MarkovChain = {
    var currentChain:MarkovChain = new EmptyChain
    val splitPattern = "(\\s|[,.!?'-:;\"])+"
    val scanner = new Scanner(new File(sourcePath))
    var line = ""
    while (scanner.hasNextLine) {
      line = scanner.nextLine()
      line.split(splitPattern).filter(_ != "").map(_.toLowerCase).foreach(word => currentChain = currentChain.addWord(word))
    }
    currentChain
  }

  val sourcePath = "ge.txt"
  val chain = feedSourceText("ge.txt")
  chain.buildNewText("2,7182818284 5904523536 0287471352 6624977572 4709369995") foreach(w => print(w+" "))
}
